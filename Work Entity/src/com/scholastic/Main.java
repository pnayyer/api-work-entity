/**
 * 
 */
package com.scholastic;

import java.util.Enumeration;

import junit.framework.TestFailure;
import junit.framework.TestResult;
import junit.framework.TestSuite;

import com.scholastic.business.ServiceEndpointTest;


public class Main {
	/**
	 * Main method; starting point of program
	 * @param args
	 */
	public static void main(String[] args) {
		//test suite object
		TestSuite suite = new TestSuite();
		//Randomly picking up 6 ids from the set provided in config.properties
		for(int counter=0; counter < 6; counter++)
			suite.addTest(TestSuite.createTest(ServiceEndpointTest.class, "checkGetEndpoint"));
	    TestResult result = new TestResult();
		//running the test suite
        suite.run(result);
        //printing result
        System.out.println("\n\nTest count :" +result.runCount() +
        		" , Success :"+(result.runCount() - result.failureCount() - result.errorCount()) +
        		" , Failure:"+ (result.failureCount() + result.errorCount()));
        //printing failures
        if(result.failureCount() > 0){
        	System.out.println(" Failure history");
        	Enumeration<TestFailure> failuresE = result.failures();
	        while(failuresE.hasMoreElements()){
	        	TestFailure failure = failuresE.nextElement();
	        	System.out.println(failure.toString());
	        }
        }
        //printing errors
        if(result.errorCount() > 0){
        	System.out.println(" Error history");
        	Enumeration<TestFailure> errorsE = result.errors();
	        while(errorsE.hasMoreElements()){
	        	TestFailure error = errorsE.nextElement();
	        	System.out.println(error.toString());
	        }
        }
	}
}
