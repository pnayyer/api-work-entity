/**
 * 
 */
package com.scholastic.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import junit.framework.TestCase;

import org.hamcrest.Matchers;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.scholastic.ConfigParser;

/**
 * Class to invoke GET service endpoint APIs
 * 
 *
 */
public class ServiceEndpointTest extends TestCase{
	
	final String className = ServiceEndpointTest.class.getName();
	List<String> idsAlreadyCovered = null;
	/**
	 * 
	 */
	public ServiceEndpointTest() {
		super();
	}
	/**
	 * setting up the test suite
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		idsAlreadyCovered = new ArrayList<String>(6);
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		idsAlreadyCovered = null;
	}
	/**
	 * API to hit get endpoint request
	 */
	public void checkGetEndpoint() {
		final String methodName ="checkGetEndpoint";
		System.out.println("\nExecuting "+className+"."+methodName);
		//fetching id randomly
		String id = null;
		do {
			id = ConfigParser.getIDs().get(new Random().nextInt(ConfigParser.getIDs().size()));
		}while(idsAlreadyCovered.contains(id));
		//forming url by combining id to URI
		String url = ConfigParser.getURL()+id;
		
		System.out.println("hitting URL "+url);
		//Invoking REST GET api
		Response resp = RestAssured
			.given()
			.when()
				.get(url);
		System.out.println("Response: "+resp.asString());
		//validating we got 200 response and not null id
		resp
			.then()
				.statusCode(200)
				.body("_id", Matchers.notNullValue());
		
		System.out.println("Execution over of "+className+"."+methodName);
	}
	
}
