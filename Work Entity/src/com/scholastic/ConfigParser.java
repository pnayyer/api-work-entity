/**
 * 
 */
package com.scholastic;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.scholastic.utils.IOUtilities;

/**
 * Class to read the the config.properties.
 * 
 */
public class ConfigParser {
	private static Properties properties = new Properties();
	static {
		readConfigFile();
	}
	
	public static boolean readConfigFile(){
		try {
			properties.load(IOUtilities.getResourceInputStream("config/config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	/**
	 * Method to get URL of GET endpoint
	 * @return
	 */
	public static String getURL(){
		return properties.getProperty("URL");
	}
	/**
	 * MEthod to get all ids seperated by comma
	 * @return
	 */
	public static List<String> getIDs(){
		String value = properties.getProperty("ID");
		String[] split = value.split(",");
		return Arrays.asList(split);
	}
	
}
